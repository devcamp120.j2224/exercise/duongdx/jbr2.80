public class App {
    public static void main(String[] args) throws Exception {
        Shape shape1 = new Shape();
        Shape shape2 = new Shape("green", false);
        System.out.println(shape1);
        System.out.println(shape2);

        System.out.println("==== Circle====");
        Circle circle1 = new Circle();
        Circle circle2 = new Circle(2);
        Circle circle3 = new Circle("green", true, 3.0);
        System.out.println(circle1);
        System.out.println(circle2);
        System.out.println(circle3);
        System.out.println("==== rectangle ====");
        Rectangle rectangle1 = new Rectangle();
        Rectangle rectangle2 = new Rectangle(2.5, 1.5);
        Rectangle rectangle3 = new Rectangle("green", true, 2.0, 1.5);
        System.out.println(rectangle1);
        System.out.println(rectangle2);
        System.out.println(rectangle3);
        System.out.println("==== square ====");
        Square square1 = new Square();
        Square square2 = new Square(1.5, 1.5);
        Square square3 = new Square("green", true, 2);
        System.out.println(square1);
        System.out.println(square2);
        System.out.println(square3);

        System.out.println("dien tich hinh 1:  " + circle1.getArea() + ",  dien tich hinh 2:  " +circle2.getArea() + ",   dien tich hinh 3:  " +circle3.getArea());
        System.out.println("Chu Vi hinh 1:  " + circle1.getPerimeter() + ",  Chu Vi hinh 2:  " +circle2.getPerimeter() + ",   Chu Vi hinh 3:  " +circle3.getPerimeter());
        System.out.println("dien tich hinh CN 1:  " + rectangle1.getArea() + ",  dien tich hinh CN 2:  " +rectangle2.getArea() + ",   dien tich hinh CN 3:  " +rectangle3.getArea());
        System.out.println("Chu Vi hinh CN 1:  " + rectangle1.getPerimeter() + ",  Chu Vi hinh CN 2:  " +rectangle2.getPerimeter() + ",   Chu Vi hinh CN 3:  " +rectangle3.getPerimeter());
        System.out.println("dien tich hinh Vuong 1:  " + square1.getArea() + ",  dien tich hinh Vuong 2:  " +square2.getArea() + ",   dien tich hinh Vuong 3:  " +square3.getArea());
        System.out.println("Chu Vi hinh Vuong 1:  " + square1.getPerimeter() + ",  Chu Vi hinh Vuong 2:  " +square2.getPerimeter() + ",   Chu Vi hinh Vuong 3:  " +square3.getPerimeter());
    }
}
