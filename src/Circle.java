public class Circle extends Shape {
    private double radius ;

    public Circle(String color, boolean filled, double radius) {
        super(color, filled);
        this.radius = radius;
    }

    public Circle(double radius) {
        this.radius = radius;
    }
    
    public Circle(){
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea(){
        return Math.PI * radius * radius ;
    }
    public double getPerimeter(){
        return Math.PI * radius * 2 ;
    }

    @Override
    public String toString() {
        return "Shape [color=" + super.getColor() + ", filled=" + super.isFilled() + "Circle [radius=" + radius + "]"; 
    }

}
