public class Square extends Rectangle{

    public Square(String color, boolean filled, double width) {
        super(color, filled);
        this.setLenght(width);
        this.setWidth(width);
    }
    

    public Square(double width, double lenght) {
        super(width, lenght);
    }


    public Square() {
    }
    

    @Override
    public String toString() {
        return "CSquare [length=" + this.getWidth()
        
        + ", area =" + this.getArea()
        + ", perimeter =" + this.getPerimeter()
        
        + "]";
    }

}
