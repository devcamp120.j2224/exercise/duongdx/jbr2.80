public class Rectangle extends Shape{
    private double width ;
    private double lenght ;
    public Rectangle(String color, boolean filled, double width, double lenght) {
        super(color, filled);
        this.width = width;
        this.lenght = lenght;
    }
    public Rectangle(double width, double lenght) {
        this.width = width;
        this.lenght = lenght;
    }
    public Rectangle(double width) {
        this.width = width;
    }
    public Rectangle() {
    }

    public Rectangle(String color, boolean filled, double width2) {
    }
    public Rectangle(String color, boolean filled) {
    }
    public double getWidth() {
        return width;
    }
    public void setWidth(double width) {
        this.width = width;
    }
    public double getLenght() {
        return lenght;
    }
    public void setLenght(double lenght) {
        this.lenght = lenght;
    }

    public double getArea(){
        return  width * lenght ;
    }
    public double getPerimeter(){
        return (width + lenght) * 2 ;
    }
    @Override
    public String toString() {
        return "Shape [color=" + super.getColor() + ", filled=" + super.isFilled() + "Rectangle [lenght=" + lenght + ", width=" + width + "]"; 
    }
    
}
